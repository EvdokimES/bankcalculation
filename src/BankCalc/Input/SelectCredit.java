package BankCalc.Input;


/**
 *  Класс выбора типа кредита
 */
public class SelectCredit {
    public static double percent;

    public static void selectCredit(){

        switch (Inputs.typeCredit){
            case 1:
                System.out.println("Вы выбрали потребительский кредит без обеспечения");
                percent=14.9;
                SayInfo.sayInfoAboutPercentCredit();
                break;
            case 2:
                System.out.println("Вы выбрали потребительский кредит под поручительство физических лиц");
                percent=13.9;
                SayInfo.sayInfoAboutPercentCredit();
                break;
            case 3:
                System.out.println("Вы выбрали кредит физическим лицам, ведущим личное подсобное хозяйство");
                percent=20;
                SayInfo.sayInfoAboutPercentCredit();
                break;
            case 4:
                System.out.println("Вы выбрали потребительский кредит военнослужащим - участникам НИС");
                percent=15;
                SayInfo.sayInfoAboutPercentCredit();
                break;
            case 5:
                System.out.println("Вы выбрали нецелевой кредит под залог недвижимости");
                percent=14;
                SayInfo.sayInfoAboutPercentCredit();
                break;
        }
    }
}
