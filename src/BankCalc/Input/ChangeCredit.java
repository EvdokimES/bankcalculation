package BankCalc.Input;

import static BankCalc.Calculation.AnnuitentСountCredit.*;
import static BankCalc.Calculation.DifferentiatedCountCredit.differentiatedCountCredit;
import static BankCalc.Input.Inputs.changeCredit;

/**
 *  Класс выбора кредита, по какой "схеме" дальше будут вычисления
 */
public class ChangeCredit {
    public static void changeCredit(){
        if (changeCredit==1) {
            annuitentCountCredit();
        }
        else if (changeCredit==2) {
            differentiatedCountCredit();
        }
    }
}
