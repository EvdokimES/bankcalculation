package BankCalc.Input;

import static BankCalc.Calculation.AnnuitentСountCredit.monthlyPayment;
import static BankCalc.Input.Inputs.termCredit;
import static BankCalc.Input.SelectCredit.percent;

/**
 *  Класс для возврата больших текстовых сообщений, чтобы не засорять класс main
 */
public class SayInfo {

    public static void sayInfoStart() {

        System.out.println("Выберете кредит:");
        System.out.println("1.Потребительский кредит без обеспечения");
        System.out.println("2.Потребительский кредит под поручительство физических лиц");
        System.out.println("3.Кредит физическим лицам, ведущим личное подсобное хозяйство");
        System.out.println("4.Потребительский кредит военнослужащим - участникам НИС");
        System.out.println("5.Нецелевой кредит под залог недвижимости");
        System.out.println("Чтобы выбрать кредит введите цифру");
        System.out.println("*************************************************************************");
    }

    public static void sayInfoFinish() {

        System.out.println("Процентная ставка на кредит:" + percent + "%");
        System.out.println("Срок кредита: " + termCredit);
        System.out.printf("Сумма ежемесячного платежа: " + "%.2f", monthlyPayment);
    }

    public static void sayInfoAboutPercentCredit() {
        System.out.println("Ставка по данному кредиту равна " + percent + "%");
    }
}
