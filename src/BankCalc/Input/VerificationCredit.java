package BankCalc.Input;

import static BankCalc.Input.Inputs.*;

/**
 * Класс проверки выбранного пользователем кредита на возможность работы с ним дальше
 */
public class VerificationCredit {
    public static void verificationCredit() {

        switch (typeCredit) {
            case 1:
                if (sum > 1500000) {
                    System.out.println("Вы не можете взять этот кредит больше чем 1500000 рублей");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                } else if (termCredit > 60) {
                    System.out.println("Вы не можете взять этот кредит больше чем на 5 лет");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                }
                break;

            case 2:
                if (sum > 3000000) {
                    System.out.println("Вы не можете взять этот кредит больше чем 3000000 рублей");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                } else if (termCredit > 60) {
                    System.out.println("Вы не можете взять этот кредит больше чем на 5 лет\nВыберете другой кредит");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                }
                break;

            case 3:
                if (termCredit > 60) {
                    System.out.println("Вы не можете взять этот кредит больше чем на 5 лет\nВыберете другой кредит");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                }
                break;

            case 4:
                if (termCredit > 60) {
                    System.out.println("Вы не можете взять этот кредит больше чем на 5 лет\nВыберете другой кредит");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                }
                break;

            case 5:
                if (termCredit > 240) {
                    System.out.println("Вы не можете взять этот кредит больше чем на 5 лет\nВыберете другой кредит");
                    Inputs.getTypeCredit();
                    VerificationCredit.verificationCredit();
                }
                break;
        }
    }
}