package BankCalc.Input;
import java.util.Scanner;

/**
 *  Класс содержит в себе возврат вводимых с клавиатуры данных пользователя
 */
public class Inputs {
    public static int sum;
    public static int termCredit;
    public static int typeCredit;
    private static int firstDeposit;
    static int changeCredit;

    public static void getSum() {
        System.out.println("Введите сумму кредита");
        Scanner in = new Scanner(System.in);
        sum = in.nextInt();
    }

    public static void getTerm() {
        System.out.println("Введите срок кредита, в месяцах");
        Scanner term = new Scanner(System.in);
        termCredit = term.nextInt();
    }

    public static void  getTypeCredit() {
        Scanner getTypeCredit = new Scanner(System.in);
        typeCredit = getTypeCredit.nextInt();
    }

    public static void getFirstDeposit() {
        System.out.println("Введите сумму первого взноса: ");
        Scanner getFirstDeposit = new Scanner(System.in);
        firstDeposit = getFirstDeposit.nextInt();
    }

    public static void getChangeCredit() {
        System.out.println("Выберете способ оплаты кредита:");
        System.out.println("1.Аннуитентный");
        System.out.println("2.Дифференцированный");
        Scanner getChangeCredit = new Scanner(System.in);
        changeCredit = getChangeCredit.nextInt();
    }
}
