
package BankCalc.Calculation;

import static BankCalc.Input.SelectCredit.percent;
import static BankCalc.Input.Inputs.sum;
import static BankCalc.Input.Inputs.termCredit;
/**
 *  Класс вычисления кредита по аннуитентной "схеме"
 */
public class AnnuitentСountCredit {

    public static double monthlyPayment;
    private static double rateMonthlyPayment;
    private static double ratePercent;
    private static double a;

    public static void annuitentCountCredit() {
        ratePercent=percent/12/100;
        a=Math.pow(1+ratePercent,termCredit);
        rateMonthlyPayment=(ratePercent*a)/(a-1);
        monthlyPayment =rateMonthlyPayment*sum;
        System.out.printf("Сумма ежемесячного платежа: " + "%.2f",monthlyPayment);
    }

}
