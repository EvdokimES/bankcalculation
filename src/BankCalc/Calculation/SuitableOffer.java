package BankCalc.Calculation;

import BankCalc.Input.Inputs;
import static BankCalc.Input.Inputs.sum;
import static BankCalc.Input.Inputs.termCredit;
import static BankCalc.Input.Inputs.typeCredit;

public class SuitableOffer {

    public static void verificationCreditOnSuitableOffer() {

        switch (typeCredit) {
            case 1:
                if (sum > 1500000) {
                } else if (termCredit > 60) {
                } else System.out.println("Потребительский кредит без обеспечения");
                break;


            case 2:
                if (sum > 3000000) {
                } else if (termCredit > 60) {
                } else System.out.println("Потребительский кредит под поручительство физических лиц");
                break;


            case 3:
                if (termCredit > 60) {
                } else System.out.println("Кредит физическим лицам, ведущим личное подсобное хозяйство");
                break;


            case 4:
                if (termCredit > 60) {
                } else System.out.println("Потребительский кредит военнослужащим - участникам НИС");
                break;


            case 5:
                if (termCredit > 240) {
                } else System.out.println("Нецелевой кредит под залог недвижимости");
                break;
        }
    }


    public static void SuitableOffer() {
        System.out.println("По введенным сумме и сроку кредита вам подходят следующие предложения:");
        for (Inputs.typeCredit = 1; Inputs.typeCredit < 6; Inputs.typeCredit++) {
            SuitableOffer.verificationCreditOnSuitableOffer();
        }
        System.out.println("*************************************************************************");
    }
}
