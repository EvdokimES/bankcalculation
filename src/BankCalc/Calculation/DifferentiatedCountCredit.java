package BankCalc.Calculation;

import static BankCalc.Input.Inputs.sum;
import static BankCalc.Input.Inputs.termCredit;
import static BankCalc.Input.SelectCredit.percent;

/**
 *  Класс вычисления кредита по дифференцированной "схеме"
 */
public class DifferentiatedCountCredit {

    private static double mainPayment;
    private static double accuredPercent;
    private static double remains;
    private static double thispayment;

    public static void differentiatedCountCredit() {

        for (int i = 0; i < termCredit; i++) {
            mainPayment = sum/termCredit;
            remains=sum-(mainPayment*i);
            accuredPercent = remains*(percent/100/12);
            thispayment = mainPayment + accuredPercent;
            System.out.printf(i+1 + " месяц: " + "%.2f", thispayment);
            System.out.println();
        }
    }
}